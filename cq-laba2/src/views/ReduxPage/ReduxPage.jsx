import React, { Fragment } from 'react'
import Button from "../../components/Button";
import { MyLink } from './styled'
class ReduxPage extends React.Component {
  state = {
    "ip": '',
    "label": '',
  };

  handleInputLabel = (e) => this.setState({label: e.target.value})
  handleInputIp = (e) => this.setState({ip: e.target.value})

  handleDeleteIp = (e) => {
        this.props.deleteIp(e.target.id)
  }

  handleAddIp = () => {
      var obj = new Object()
      obj.ip=   this.state.ip;
      obj.label=this.state.label;
    this.props.addIp(obj)
  }

  render() {
    return (
      <Fragment>
        <h1>Redux</h1>

        <Fragment>
          <h2>Add new IP address</h2>
          <Fragment>
            <div>
              <p><b>IP address</b></p>
              <input type='text' value={this.state.ip} onChange={this.handleInputIp}   />
            </div>
            <div>
              <p><b>Label</b></p>
              <input type='text' value={this.state.label} onChange={this.handleInputLabel}/>

            </div>
            <p>
              <Button
                type='primary'
                onClick={this.handleAddIp}
              >
                Add
              </Button>
            </p>
          </Fragment>
        </Fragment>


        <Fragment>



            {Object.keys(this.props.ips).map(key => <div key={key}>{key} - {this.props.ips[key].ip} - {this.props.ips[key].label} - <a  onClick={this.handleDeleteIp} id={key}>удалить</a></div>)}


        </Fragment>

      </Fragment>
    )
  }
}

export default ReduxPage
