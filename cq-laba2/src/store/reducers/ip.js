import {
    IP_LIST,
    IP_ADD,
    IP_DELETE
} from '../types'
import {store} from "../index";

const initialState = (localStorage['redux-store']) ? JSON.parse(localStorage['redux-store']) : {}






export default (state = initialState, action) => {
    switch (action.type) {
        case IP_LIST:
            return state
        case IP_ADD:
            //вычисляем максимум ключ и прибавляем +1
            let arr=Object.keys(state)
            let max=0
            for(let i =0; i < arr.length; i++){
                if (Number(arr[i]) >= max) {max = Number(arr[i])
                }
            }

            let id=max+1;

            state[id]=  action.payload;

            return {
                ...state,
            }
        case IP_DELETE:
            delete state[action.payload]
            console.log(action.payload)
            console.log(['Delete to be implemented'])
            return {
                ...state,
            }
        default:
            return state
    }
}
